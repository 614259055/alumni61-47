<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Alumni extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Menu_model', 'Menu');
	}

	public function index()
	{
		$this->load->view('index');
	}



	public function login()
	{
		$this->load->view('login');
	}

	public function find()
	{
		$result['user'] = $this->Menu->showAlumni();
		$this->load->view('find', $result);
	}

	public function information()
	{
		$result['user'] = $this->Menu->showDataAlumni();
		$this->load->view('information', $result);
	}

	public function profile()
	{
		$result['user'] = $this->Menu->showDataAlumni();
		$this->load->view('profile', $result);
	}

	public function search()
	{
		// $data = $this->input->post();

		// $data = $this->Manu->getSearchUsers($data);

		// echo json_encode($data);

		$data = $this->input->post('searchalumni');
		$result['user'] = $this->Menu->getSearchUsers($data);
		$this->load->view('search', $result);
		// $this->session->set_flashdata('result',$result);
		// redirect('Alumni/alumnisearch', 'refresh');
	}

	public function test()
	{
		// $postData = $this->input->post();

		// $data = $this->Manu->getSearchUsers($postData);

		// echo json_encode($data);
		$this->load->view('test');
	}
	public function reg(){

		$this->load->view('register');
	}
	// public function showAll()
	// {
		public function register(){
			$data = array(
				's_gender' => $this->input->post("s_gender"),
				's_title' => $this->input->post("s_title"),
				's_fname' => $this->input->post("s_fname"),
				's_lname'=> $this->input->post("s_lname"),
				's_nickname'=> $this->input->post("s_nickname"),
				's_bday'=> $this->input->post("s_bday"),
				's_facebook'=> $this->input->post("s_facebook"),
				's_email'=> $this->input->post("s_email"),
				's_password'=> $this->input->post("s_password"),
				's_repassword'=> $this->input->post("s_repassword"),
				'address1'=> $this->input->post("address1"),
				'address2'=> $this->input->post("address2"),
				'address3'=> $this->input->post("address3"),
				'address4'=> $this->input->post("address4"),
				'address5'=> $this->input->post("address5"),
				's_generation'=> $this->input->post("s_generation"),
				's_address'=> $this->input->post("s_address"),
				's_position'=> $this->input->post("s_position"),
				's_phone'=> $this->input->post("s_phone"),
		
			);
				// $this->Menu->menu_insert($data);
				$this->Menu->Alumniw($data);
				$this->load->view('register');
				// $this->load->view('view_insert_successwithmenulink');
			  
		
		}
	// }
}
